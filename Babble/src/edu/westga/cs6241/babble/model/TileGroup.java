package edu.westga.cs6241.babble.model;

import java.util.ArrayList;

public abstract class TileGroup {

  private ArrayList<Tile> tiles;
  
/**
 * This is the constructor for the tileGroup
 */
  public TileGroup()
  {
    this.tiles = new ArrayList();
  }
/**
 * returns the tiles in the array list
 * @return the tiles in the array list
 */
  protected ArrayList<Tile> getTiles()
  {
    return this.tiles;
  }
/**
 * adds a tile
 * @param tile
 */
  public void append(Tile tile)
  {
    this.tiles.add(tile);
  }
/**
 * removes a tile
 * @param tile
 */
  protected void remove(Tile tile)
  {
    this.tiles.remove(tile);
  }
/**
 * returns the letter as a string
 */
  public String toString()
  {
    String s = "";
    for (Tile x : this.tiles)
    {
      s = s + x.getLetter();
    }
    return s;
  }
}