package edu.westga.cs6241.babble.model;

public class Word extends TileGroup {
/**
 * Gets score from words created
 * @return score
 */
  public int getScore()
  {
    int score = 0;
    for (Tile t : super.getTiles())
    {
      score += t.getPointValue();
    }
    return score;
  }
  
  
}
