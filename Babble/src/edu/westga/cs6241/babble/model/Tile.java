package edu.westga.cs6241.babble.model;
/**
 *This is the tile class. that will get the letter and the value of that letter
 *@author Jonathan Greene
 *@version 10/5/2012
 */
public class Tile {
	char letter;
	int	pointValue;
	
	/**
	 * This is the constructor for Tile
	 */
	public Tile(char letter, int pointValue) {
	
		if ((letter < 'A') || (letter > 'Z')) 
		{
			throw new IllegalArgumentException("letter needs to be between A and Z");
		}
	
		if (pointValue <= 0)
		{
			throw new IllegalArgumentException("pointValue must be greater than 0");
		}
		
		this.letter = letter;
		this.pointValue = pointValue;
	}
	/**
	 * gets the letter character
	 * @return the letter
	 */
	public char getLetter() {
		return this.letter;
	}
	/**
	 * gets the value
	 * @return the value
	 */
	public int getPointValue() {
		return this.pointValue;
	}
	
}

