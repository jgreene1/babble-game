package edu.westga.cs6241.babble.test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import edu.westga.cs6241.babble.model.Tile;
import edu.westga.cs6241.babble.model.TileRack;
import edu.westga.cs6241.babble.model.Word;

public class WhenTestWord {

	private TileRack gRack;
	
	@Before
	public void setUpGetScoreHeavy()
	throws Exception
	{
		this.gRack = new TileRack(7);
		
		this.gRack.append(new Tile('H', 4));
		this.gRack.append(new Tile('E', 1));
		this.gRack.append(new Tile('A', 1));
		this.gRack.append(new Tile('V', 4));
		this.gRack.append(new Tile('Y', 4));
		this.gRack.append(new Tile('A', 1));
		this.gRack.append(new Tile('A', 1));
		
	}
	
	@After
	public void tearDownGetScore() throws Exception {
		
	}
	
	
	
	/** 
	 * Checks to make sure that the point values are adding
	 */
	@Test
	public void shouldHaveScore14ForHeavy() {
		Word word = this.gRack.removeWord("HEAVY");
		Assert.assertEquals(14, word.getScore());
	}
	
	
	/**
	 * Checks to make sure that a word object with no tiles gives a score of 0
	 */
	@Test
	public void emptyWordShouldBe0() {
		Word word = new Word();
		Assert.assertEquals(0, word.getScore());
	}
	
}


