package edu.westga.cs6241.babble.test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import edu.westga.cs6241.babble.model.Tile;
import edu.westga.cs6241.babble.model.TileBag;
import edu.westga.cs6241.babble.model.TileRack;
import edu.westga.cs6241.babble.model.Word;
/**
 * These are the test for the tile rack 
 * @author Jonathan Greene
 * @version 11/19/2014
 *
 */
public class WhenTestTileRack {
	
	private TileRack rack;
    private TileRack eRack;
    private	TileRack dRack;
    
	
	@Before
	public void setUp()
	throws Exception
	{
		this.rack = new TileRack(7);
		
		this.rack.append(new Tile('H', 1));
		this.rack.append(new Tile('E', 1));
		this.rack.append(new Tile('A', 1));
		this.rack.append(new Tile('V', 1));
		this.rack.append(new Tile('Y', 1));
		this.rack.append(new Tile('H', 1));
		this.rack.append(new Tile('X', 1));
		
	}
	
	@After
	public void tearDown() throws Exception {
		
	}
	
	/**
	 * this test should show the rack contains HEAVY
	 */
	
	@Test
	public void shouldContainHEAVY() {
		Assert.assertTrue(this.rack.canMakeWordFrom("HEAVY"));
		
	}
	
	/**
	 * this is a test to show that the tile rack should not contain wet
	 */
	@Test
	public void shouldNotContainWET() {
		Assert.assertFalse(this.rack.canMakeWordFrom("WET"));
	}
	
	/**
	 * This test should be able to remove the word HEAVY
	 */
	@Test
	public void shouldBeAbleToRemoveHEAVY() {
		Word word = this.rack.removeWord("HEAVY");
		Assert.assertTrue(word.toString().equals("HEAVY"));
		Assert.assertEquals(this.rack.getNumberOfTilesNeeded(), 5L);
		Assert.assertTrue(this.rack.canMakeWordFrom("HX"));
	}
	
	/**
	 * This test the toString method to check and see if it is HEAVYHX
	 */
	@Test
	public void toStringShouldBeHEAVYHX() {
	 Assert.assertEquals(this.rack.toString(), "HEAVYHX");
	}
	

	
	@Before
	public void setUpEmpty()
	throws Exception
	{
		this.eRack = new TileRack(1);
	}
		
	@After
	public void tearDownEmpty() throws Exception {
		
	}
	
	/**
	 * This test to make sure an empty rack can not contain the word DWELL
	 */
	@Test
	public void shouldNotContainDWELL() {
		Assert.assertFalse(this.eRack.canMakeWordFrom("DWELL"));
	}
	
	@Before
	public void setUpDuplicate()
	throws Exception {
		
	
		this.dRack = new TileRack(7);
		
		this.dRack.append(new Tile('C', 1));
		this.dRack.append(new Tile('A', 1));
		this.dRack.append(new Tile('T', 1));
		this.dRack.append(new Tile('A', 1));
		this.dRack.append(new Tile('A', 1));
		this.dRack.append(new Tile('A', 1));
		this.dRack.append(new Tile('A', 1));
		
	}
	
	@After
	public void tearDownDuplicate() throws Exception {
		
	}
	
	/**
	 * This test checks to make sure it does not remove duplicate tiles
	 */
	@Test
	public void shouldNotRemoveDuplicateTiles() {
		Word word = this.dRack.removeWord("CAT");
		Assert.assertTrue(word.toString().equals("CAT"));
		Assert.assertEquals(this.dRack.getNumberOfTilesNeeded(), 3L);
		Assert.assertTrue(this.dRack.canMakeWordFrom("AAAA"));
	}
	
	@Before
	public void setUpOutOrder()
	throws Exception
	{
		this.dRack = new TileRack(7);
		
		this.dRack.append(new Tile('T', 1));
		this.dRack.append(new Tile('A', 1));
		this.dRack.append(new Tile('A', 1));
		this.dRack.append(new Tile('C', 1));
		this.dRack.append(new Tile('A', 1));
		this.dRack.append(new Tile('A', 1));
		this.dRack.append(new Tile('A', 1));
		
	}
	
	@After
	public void tearDownOutOrder() throws Exception {
		
	}
	
	/** 
	 * This checks to see if it can get a word if its not in the right order
	 */
	@Test
	public void canRemoveWordOutOfOrder() {
		Word word = this.dRack.removeWord("CAT");
		Assert.assertTrue(word.toString().equals("CAT"));
		Assert.assertEquals(this.dRack.getNumberOfTilesNeeded(), 3L);
		Assert.assertTrue(this.dRack.canMakeWordFrom("AAAA"));
	}
	
}
	
	
	

	
	
	
