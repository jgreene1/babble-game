package edu.westga.cs6241.babble.test;
/**
 * These are the test for the tile bag
 * @author Jonathan Greene
 * @version 11/19/2014
 */
import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import edu.westga.cs6241.babble.model.TileBag;

public class WhenTestTileBag {
	
	private TileBag tileBag;
	
	@Before
	public void setUp() {
		this.tileBag = new TileBag();
	}
	@After
	public void tearDown() {
		
	}
	
	/**
	 * This test checks to see if you can draw 98 times
	 */
	@Test
	public void canDrawAllTiles() {
		for (int i = 0; i < 98; i++)
			try {
				this.tileBag.drawTile();
			} catch (IllegalArgumentException e) {
				org.junit.Assert.fail();
			}
		Assert.assertTrue(this.tileBag.isEmpty());
	}
	
	/**
	 * This test checks to make sure you can not draw 100 tiles
	 */
	@Test
	public void canNotDraw100Tiles() {
		try {
			for (int i = 0; i < 100; i++) this.tileBag.drawTile();
			org.junit.Assert.fail();
		}
		catch (IllegalStateException I) {
			org.junit.Assert.assertTrue(true);
		}
	}
}
