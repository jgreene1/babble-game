package edu.westga.cs6241.babble.test;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Test;

import edu.westga.cs6241.babble.model.Tile;

public class whenTestTile {

	/**
	 * This test to make sure you can not have a tile with point value of 0
	 */
	@Test
	public void canNotMake0PointTile() {
		try {
			Tile tile = new Tile('H', 0);
		
		} 
		catch(IllegalArgumentException ex) {
			Assert.assertEquals("pointValue must be greater than 0", ex.getMessage());
		}
		
	}
	
	/**
	 * this test checks to make sure you can not pass a negative value to a tile
	 */
	@Test
	public void canNotMakeNegative4PointTile() {
		try {
			Tile tile = new Tile('H', -4);
		}
		catch(IllegalArgumentException ex) {
			Assert.assertEquals("pointValue must be greater than 0", ex.getMessage());
		}
		
	}
	
	/**
	 * This test checks to make sure you can not make a lower case tile
	 */
	@Test
	public void canNotMakeLowerCaseHTile() {
		try {
			Tile tile = new Tile('h', 4);
		}
		catch(IllegalArgumentException ex) {
			Assert.assertEquals("letter needs to be between A and Z", ex.getMessage());
		}
		
	}
	
	/**
	 * This test checks to see if you can set a tile with value F and point 4
	 */
	@Test
	public void canMake4PointFTile() {
		Tile tile = new Tile('F', 4);
		Assert.assertEquals(tile.getLetter(), 'F');
		Assert.assertEquals(tile.getPointValue(), 4);
		
	}

}
