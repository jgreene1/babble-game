package edu.westga.cs6241.babble;

import java.awt.Dimension;

import edu.westga.cs6241.babble.views.*;

/**
 * Main class for starting a Babble game. 
 * @author Jonathan Greene
 * @version 11/5/2014
 *
 */
public class Babble {

	public static void main(String[] args) throws Exception {
		BabbleTUI tui = new BabbleTUI();
		tui.run();
	}

}
